
function bodyOnLoad() {
    var screenWidth = screen.width;
    var screenHeight = screen.height;
    window.JSInterface.setScreenSize(screenWidth, screenHeight);

	var images = document.getElementsByTagName("img");
	for(var i = 0; i < images.length; i++){
	    var image = images[i];

	    var src = image.src;
	    var width = image.width;
	    var height = image.height;
	    var x = image.x;
	    var y = image.y;
	    window.JSInterface.createHapticSprite(src, width, height, x, y);
	}
};