package co.tanvas.modulator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;

import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import co.tanvas.haptics.service.adapter.HapticDebugView;
import co.tanvas.haptics.service.adapter.HapticServiceAdapterEventListener;
import co.tanvas.haptics.service.adapter.iHapticServiceAdapterEventHandler;
import co.tanvas.haptics.service.app.HapticApplication;
import co.tanvas.haptics.service.model.HapticManager;
import co.tanvas.haptics.service.model.HapticMaterial;
import co.tanvas.haptics.service.model.HapticSprite;
import co.tanvas.haptics.service.model.HapticTexture;

public class MainActivity extends Activity implements iHapticServiceAdapterEventHandler {

    private static final String TEST_PAGE_URL = "http://10.0.9.14/modulator/index.html";

    private HapticManager mHapticManager;
    private WebView mWebView;
    private HapticDebugView mDebugView;

    private ArrayList<HapticSprite> mSprites = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Remove title bar
        this.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        // Remove notification bar
        this.getWindow().setFlags(android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN,
                android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        // Set up web view
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.addJavascriptInterface(new JavaScriptInterface(this), "JSInterface");
        mWebView.loadUrl(TEST_PAGE_URL);
        mWebView.setAlpha(0.0f);
//        mWebView.setVisibility(View.GONE);

        // Set up debug view
        mDebugView = (HapticDebugView) findViewById(R.id.debug_view);

        // Add haptics handler
        HapticServiceAdapterEventListener.obtain(this).addHandler(this);
    }

    public void onRefresh(View view) {
        try {
            destroyNativeResources();
            mHapticManager.destroyAllSpritesAndResources();
        } catch (Exception e) {
            Log.e("Reload", e.toString());
        }

        mDebugView.removeAllSprites();
        mWebView.reload();
    }

    protected void destroyNativeResources() {
        try {
            if (mSprites != null && mSprites.size() > 0) {
                for (HapticSprite sprite : mSprites) {
                    if (sprite != null) {
                        if (sprite.getMaterial() != null) {
                            if (sprite.getMaterial().getTextures() != null) {
                                mHapticManager.getServiceAdapter().destroyTexture(sprite.getMaterial().getTextures()[0].getNativeId());
                            }
                            mHapticManager.getServiceAdapter().destroyMaterial(sprite.getMaterial().getNativeId());
                        }
                        mHapticManager.getServiceAdapter().getConnector().getBinder().removeSprite(mHapticManager.getNativeId(), sprite.getNativeId());
                        mHapticManager.getServiceAdapter().destroySprite(sprite.getNativeId());
                    }
                }
            }

            mSprites.clear();
        } catch (Exception e) {
            Log.e("Tanvas", "Got exception " + e.toString());
        }
    }

    @Override
    public void serviceAdapterIsConnected(Intent intent) {
        initHaptics();
    }

    @Override
    public void serviceAdapterIsDisconnected(Intent intent) {

    }

    @Override
    public void serviceAdapterIsConnecting(Intent intent) {
    }

    @Override
    public void serviceAdapterIsDisconnecting(Intent intent) {
    }

    @Override
    public void serviceAdapterWasCreated(Intent intent) {
    }

    private void initHaptics() {
        try {
            if (mHapticManager == null) {
                mHapticManager = HapticManager.create(HapticApplication.getHapticServiceAdapter());
                mHapticManager.setTransform((float) (Math.PI / 2), -1, -1, 0, 1536);
//                mHapticManager.hapticDebugView = mDebugView;
            }
            mHapticManager.activate();
        } catch (Exception e) {
            Log.e(null, "initHaptics() threw exception " + e.toString());
        }
    }

    protected HapticSprite createHapticSpriteFromUrl(String urlStr) {
        Bitmap hapticBitmap = null;
        // Create bitmap from url
        try {
            URL url = new URL(urlStr);
            hapticBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.e("CreateHapticSpriteURL", e.toString());
        }

        // Initialize texture data from bitmap resource id
        int byteCount = hapticBitmap.getByteCount();
        int textureDataWidth = hapticBitmap.getRowBytes() / 4; // ARGB
        int textureDataHeight = hapticBitmap.getHeight();
        byte[] textureData = new byte[textureDataWidth * textureDataHeight];
        ByteBuffer buffer = ByteBuffer.allocate(byteCount);
        hapticBitmap.copyPixelsToBuffer(buffer);
        byte[] tempData = buffer.array();

        for (int j = 0; j < textureDataHeight; j++) {
            for (int i = 0; i < textureDataWidth; i++) {
                textureData[j * textureDataWidth + i] = tempData[(j * textureDataWidth + i) * 4 + 1];
            }
        }

        // Initialize haptic sprite from texture data
        HapticSprite hapticSprite = null;
        HapticMaterial hapticMaterial;
        HapticTexture hapticTexture;

        try {
            hapticSprite = HapticSprite.create(HapticApplication.getHapticServiceAdapter());
            hapticMaterial = HapticMaterial.create(HapticApplication.getHapticServiceAdapter());
            hapticTexture = HapticTexture.create(HapticApplication.getHapticServiceAdapter());

            hapticTexture.setSize(textureDataWidth, textureDataHeight);
            hapticTexture.setData(textureData);
            hapticMaterial.setTexture(0, hapticTexture);
            hapticSprite.setMaterial(hapticMaterial);
        } catch (Exception e) {
            Log.e("Error", e.toString());
        } finally {
            hapticBitmap.recycle();
        }

        return hapticSprite;
    }

    /**
     * The JavaScript interface for communicating with the contents in the {@link MainActivity#mWebView}.
     */
    final class JavaScriptInterface {
        private Context context;

        public JavaScriptInterface(Context context) {
            this.context = context;
        }

        @JavascriptInterface
        public void setScreenSize(int screenWidth, int screenHeight) {
            Log.e("size", screenWidth + " " + screenHeight);
        }

        @JavascriptInterface
        public void createHapticSprite(String src, float width, float height, float x, float y) {
            Log.e("Src", src);
            Log.e("Width", width + "");
            Log.e("Height", height + "");
            Log.e("X", x + "");
            Log.e("Y", y + "");

            HapticSprite sprite = createHapticSpriteFromUrl(src);
            try {
                float spriteWidth = width;
                float spriteHeight = height;
                int spriteX = (int) (x);
                int spriteY = (int) (y);
                sprite.setSize(spriteWidth, spriteHeight);
                sprite.setPosition(spriteX, spriteY);
                mHapticManager.addSprite(sprite);
                mSprites.add(sprite);
                mDebugView.addSprite(sprite);
            } catch (Exception e) {
                Log.e("CreateHapticSprite", e.toString());
            }
        }
    }
}
